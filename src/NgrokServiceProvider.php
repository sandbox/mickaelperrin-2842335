<?php

namespace Drupal\ngrok;

use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ContainerBuilder;

/**
 * Class NgrokServiceProvider.
 *
 * Override the session_configuration service.
 *
 * @package Drupal\ngrok
 */
class NgrokServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('session_configuration')) {
      // Load the definition of the session_configuration servie.
      $definition = $container->getDefinition('session_configuration');
      // Override the default class, by our own.
      $definition->setClass(
        '\Drupal\ngrok\NgrokSessionConfiguration'
      );
    }
    if ($container->hasDefinition('url_generator.non_bubbling')) {
      $definition = $container->getDefinition('url_generator.non_bubbling');
      $definition->setClass(
        '\Drupal\ngrok\NgrokUrlGenerator'
      );
    }
  }

}
